#include <math.h>

#include "rtc_date.h"
#include "rtc_time.h"

#include <Wire.h>

static const unsigned long SERIAL_BAUD = 115200;
static const uint32_t WIRE_CLOCK_SPEED = 400000;

static const uint16_t BASE_YEAR = 2000;

static const size_t YEAR_CHAR = 7;
static const size_t MONTH_CHAR = 0;
static const size_t DAY_CHAR = 4;
static const size_t HOURS_CHAR = 0;
static const size_t MINUTES_CHAR = 3;
static const size_t SECONDS_CHAR = 6;

static const size_t YEAR_LENGTH = 4;
static const size_t DAY_LENGTH = 2;
static const size_t HOURS_LENGTH = 2;
static const size_t MINUTES_LENGTH = 2;
static const size_t SECONDS_LENGTH = 2;

static RTC::Date date;
static RTC::Time time;

uint8_t getBuildMonth() {
  const char *monthChar = &__DATE__[MONTH_CHAR];

  switch (monthChar[0]) {
    case 'J':
      if (monthChar[1] == 'a') {
        return RTC::Date::MONTH_JANUARY;
      } else {
        return monthChar[2] == 'n' ? RTC::Date::MONTH_JUNE : RTC::Date::MONTH_JULY;
      }
    case 'F':
      return RTC::Date::MONTH_FEBRUARY;
    case 'M':
      return monthChar[2] == 'r' ? RTC::Date::MONTH_MARCH : RTC::Date::MONTH_MAY;
    case 'A':
      return monthChar[1] == 'p' ? RTC::Date::MONTH_APRIL : RTC::Date::MONTH_AUGUST;
    case 'S':
      return RTC::Date::MONTH_SEPTEMBER;
    case 'O':
      return RTC::Date::MONTH_OCTOBER;
    case 'N':
      return RTC::Date::MONTH_NOVEMBER;
    case 'D':
      return RTC::Date::MONTH_DECEMBER;
    default:
      return 0;
  }
}

uint8_t getBuildWeekday() {
  int Y = getBuildYear();
  if (getBuildMonth() == RTC::Date::MONTH_JANUARY || getBuildMonth() == RTC::Date::MONTH_FEBRUARY) {
    Y -= 1;
  }

  int y = Y % 100;
  int c = Y / 100;
  int d = getBuildDay();
  int m = (getBuildMonth() + 10) % 12;
  if (getBuildMonth() == 0) {
    m = 12;
  }

  int w = d;
  w += static_cast<int>(floor(2.6 * m - 0.2));
  w += y;
  w += y / 4;
  w += c / 4;
  w -= 2 * c;
  w %= 7;
  if (w < 0) {
    w += 7;
  }
  w += 1;

  return w;
}

template<typename T>
auto parseNumber(const char *number, size_t length) -> T {
  T value = 0;
  for (size_t i = 0; i < length; ++i) {
    value *= 10;
    if (number[i] == ' ') {
      continue;
    }
    value += number[i] - '0';
  }
  return value;
}

uint16_t getBuildYear() {
  return parseNumber<uint16_t>(&__DATE__[YEAR_CHAR], YEAR_LENGTH);
}

uint8_t getBuildDay() {
  return parseNumber<uint8_t>(&__DATE__[DAY_CHAR], DAY_LENGTH);
}

uint8_t getBuildHours() {
  return parseNumber<uint8_t>(&__TIME__[HOURS_CHAR], HOURS_LENGTH);
}

uint8_t getBuildMinutes() {
  return parseNumber<uint8_t>(&__TIME__[MINUTES_CHAR], MINUTES_LENGTH);
}

uint8_t getBuildSeconds() {
  return parseNumber<uint8_t>(&__TIME__[SECONDS_CHAR], SECONDS_LENGTH);
}

void printDate(uint16_t year, uint8_t month, uint8_t day, uint8_t weekday) {
  switch (weekday) {
    case RTC::Date::WEEKDAY_SUNDAY:
      Serial.print(F("Sunday"));
      break;
    case RTC::Date::WEEKDAY_MONDAY:
      Serial.print(F("Monday"));
      break;
    case RTC::Date::WEEKDAY_TUESDAY:
      Serial.print(F("Tuesday"));
      break;
    case RTC::Date::WEEKDAY_WEDNESDAY:
      Serial.print(F("Wednesday"));
      break;
    case RTC::Date::WEEKDAY_THURSDAY:
      Serial.print(F("Thursday"));
      break;
    case RTC::Date::WEEKDAY_FRIDAY:
      Serial.print(F("Friday"));
      break;
    case RTC::Date::WEEKDAY_SATURDAY:
      Serial.print(F("Saturday"));
      break;
    default:
      Serial.print(date.getWeekday());
      break;
  }

  Serial.print(", ");

  switch (month) {
    case RTC::Date::MONTH_JANUARY:
      Serial.print(F("January"));
      break;
    case RTC::Date::MONTH_FEBRUARY:
      Serial.print(F("February"));
      break;
    case RTC::Date::MONTH_MARCH:
      Serial.print(F("March"));
      break;
    case RTC::Date::MONTH_APRIL:
      Serial.print(F("April"));
      break;
    case RTC::Date::MONTH_MAY:
      Serial.print(F("May"));
      break;
    case RTC::Date::MONTH_JUNE:
      Serial.print(F("June"));
      break;
    case RTC::Date::MONTH_JULY:
      Serial.print(F("July"));
      break;
    case RTC::Date::MONTH_AUGUST:
      Serial.print(F("August"));
      break;
    case RTC::Date::MONTH_SEPTEMBER:
      Serial.print(F("September"));
      break;
    case RTC::Date::MONTH_OCTOBER:
      Serial.print(F("October"));
      break;
    case RTC::Date::MONTH_NOVEMBER:
      Serial.print(F("Novemeber"));
      break;
    case RTC::Date::MONTH_DECEMBER:
      Serial.print(F("December"));
      break;
    default:
      Serial.print(date.getMonth());
      break;
  }

  Serial.print(" ");
  Serial.print(day);
  Serial.print(", ");
  Serial.print(year);
}

void printTime(uint8_t hours, uint8_t minutes, uint8_t seconds) {
  if (hours < 10) {
    Serial.print("0");
  }
  Serial.print(hours);
  Serial.print(":");
  if (minutes < 10) {
    Serial.print("0");
  }
  Serial.print(minutes);
  Serial.print(":");
  if (seconds < 10) {
    Serial.print("0");
  }
  Serial.print(seconds);
}

void setup() {
  Wire.begin();
  Wire.setClock(WIRE_CLOCK_SPEED);

  Serial.begin(SERIAL_BAUD);
  while (!Serial) {}

  RTC::DS3231::setup();
  date.setNow(getBuildYear() - BASE_YEAR, getBuildMonth(), getBuildDay(), getBuildWeekday());
  time.setNow(getBuildHours(), getBuildMinutes(), getBuildSeconds());
}

void loop() {
  date.getNow();
  time.getNow();
  Serial.print(__DATE__);
  Serial.print(" ");
  Serial.print(__TIME__);
  Serial.println();

  printDate(getBuildYear(), getBuildMonth(), getBuildDay(), getBuildWeekday());
  Serial.print(" ");
  printTime(getBuildHours(), getBuildMinutes(), getBuildSeconds());
  Serial.println();

  printDate(date.getYear(), date.getMonth(), date.getDay(), date.getWeekday());
  Serial.print(" ");
  printTime(time.getHours(), time.getMinutes(), time.getSeconds());
  Serial.println();

  Serial.println();

  delay(5000);
}
