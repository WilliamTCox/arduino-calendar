#include "rtc_date.h"
#include "rtc_time.h"
#include "rtc_alarm.h"

#include "epd_display.h"

#include "body.h"
#include "months.h"
#include "numbers.h"
#include "weekdays.h"
#include "symbols.h"

#include <Wire.h>
#include <avr/sleep.h>
#include <util/atomic.h>

#include <string.h>


static const unsigned long SERIAL_BAUD      = 115200;
static const uint32_t      WIRE_CLOCK_SPEED = 400000;
static const uint8_t       WAKE_UP_PIN      = 2;

static RTC::Date   date;
static RTC::Time   time;
static RTC::Alarm1 alarm;

static EPD::Display display;

static Images::BODY     imagesBody;
static Images::MONTHS   imagesMonths;
static Images::NUMBERS  imagesNumbers;
static Images::SYMBOLS  imagesSymbols;
static Images::WEEKDAYS imagesWeekdays;


class ImageChooser {
protected:
  static const uint8_t MIN_BEFORE_REPEAT = 31;

  uint8_t images[MIN_BEFORE_REPEAT];

  uint8_t nextWithoutRepeat(uint8_t size = MIN_BEFORE_REPEAT) {
    uint8_t next         = random(Images::BODY::COUNT - size);
    uint8_t lessThanNext = 0;

    for (int i = 0; i < size; ++i) {
      if (images[i] <= next) {
        ++lessThanNext;
      }
    }

    return next + lessThanNext;
  }

public:
  void setup() {
    for (int i = 0; i < MIN_BEFORE_REPEAT; ++i) {
      images[i] = nextWithoutRepeat(i);
    }
  }

  uint8_t nextImage() {
    uint8_t next = nextWithoutRepeat();

    memmove(&images[1], &images[0], sizeof(images) - sizeof(images[0]));
    images[0] = next;

    return images[0];
  }
};

static ImageChooser imageChooser;


uint16_t drawImage(uint16_t x, uint16_t y, const Images::Image &images, uint8_t index) {
  display.drawImage(x, y, images.filename(index));
  return images.width(index);
}

uint16_t dateWidth() {
  uint16_t width = 0;

  width += imagesWeekdays.width(date.getWeekday() - 1);
  width += imagesMonths.width(date.getMonth() - 1);
  width += imagesSymbols.width(imagesSymbols.SPACE);
  uint8_t day10 = date.getDay10();
  if (day10 > 0) {
    width += imagesNumbers.width(day10);
  }
  width += imagesNumbers.width(date.getDay1());
  width += imagesSymbols.width(imagesSymbols.COMMA);
  width += imagesSymbols.width(imagesSymbols.SPACE);
  width += imagesNumbers.width(2);
  width += imagesNumbers.width(0);
  width += imagesNumbers.width(date.getYear10());
  width += imagesNumbers.width(date.getYear1());

  return width;
}

void displayDate(uint16_t y = Images::LINE_HEIGHT / 2) {
  date.getNow();

  uint16_t x = (EPD::Display::WIDTH - imagesSymbols.width(imagesSymbols.LINE)) / 2;
  drawImage(x, y + Images::LINE_HEIGHT * 2 / 3, imagesSymbols, imagesSymbols.LINE);

  x = (EPD::Display::WIDTH - dateWidth()) / 2;

  x += drawImage(x, y, imagesWeekdays, date.getWeekday() - 1);
  x += drawImage(x, y, imagesMonths, date.getMonth() - 1);
  x += imagesSymbols.width(imagesSymbols.SPACE);
  uint8_t day10 = date.getDay10();
  if (day10 > 0) {
    x += drawImage(x, y, imagesNumbers, day10);
  }
  x += drawImage(x, y, imagesNumbers, date.getDay1());
  x += drawImage(x, y, imagesSymbols, imagesSymbols.COMMA);
  x += imagesSymbols.width(imagesSymbols.SPACE);
  x += drawImage(x, y, imagesNumbers, 2);
  x += drawImage(x, y, imagesNumbers, 0);
  x += drawImage(x, y, imagesNumbers, date.getYear10());
  x += drawImage(x, y, imagesNumbers, date.getYear1());
}

void displayBody(uint16_t y = Images::LINE_HEIGHT * 2) {
  drawImage(0, y, imagesBody, imageChooser.nextImage());
}

void wakeUp() {
  detachInterrupt(digitalPinToInterrupt(WAKE_UP_PIN));
}

void sleepNow() {
  Serial.flush();

  ATOMIC_BLOCK(ATOMIC_FORCEON) {
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    attachInterrupt(digitalPinToInterrupt(WAKE_UP_PIN), wakeUp, LOW);

    sleep_enable();
    sleep_bod_disable();

    NONATOMIC_BLOCK(NONATOMIC_FORCEOFF) {
      sleep_cpu();
    }

    sleep_disable();
  }
}

void setupRandom() {
  time.getNow();
  date.getNow();

  unsigned long seed = 0;

  unsigned char *seedByte = reinterpret_cast<unsigned char *>(&seed);

  switch (sizeof(seed)) {
    default:
      seedByte[5] = date.getYearByte();
    case 5:
      seedByte[4] = date.getMonthByte();
    case 4:
      seedByte[3] = date.getDayByte();
    case 3:
      seedByte[2] = time.getHoursByte();
    case 2:
      seedByte[1] = time.getMinutesByte();
    case 1:
      seedByte[0] = time.getSecondsByte();
    case 0:
      break;
  }

  randomSeed(seed);
}

void setup() {
  pinMode(WAKE_UP_PIN, INPUT);

  Wire.begin();
  Wire.setClock(WIRE_CLOCK_SPEED);

  Serial.begin(SERIAL_BAUD);
  while (!Serial) {}

  RTC::DS3231::setup();
  EPD::WS43::setup();

  display.wakeUp();
  display.setStorage(EPD::Display::STORAGE_TF);

  setupRandom();
  imageChooser.setup();
}

void loop() {
  display.setColor(EPD::Display::COLOR_BLACK, EPD::Display::COLOR_WHITE);
  display.clear();
  display.setEnglishFont(EPD::Display::FONT_32);

  displayDate();
  displayBody();

  display.update();
  display.sleep();

  alarm.matchHours(0, 0, 0);
  sleepNow();
  alarm.clear();

  display.wakeUp();
}
