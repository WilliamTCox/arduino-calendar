#ifndef H_RTC_DATE
#define H_RTC_DATE

#include "rtc_ds3231.h"

#include <inttypes.h>

namespace RTC {
class Date : public DS3231 {
  private:
    uint8_t year;
    uint8_t month;
    uint8_t day;
    uint8_t weekday;

  public:

    enum MONTH_t : uint8_t {
      MONTH_JANUARY = 1,
      MONTH_FEBRUARY,
      MONTH_MARCH,
      MONTH_APRIL,
      MONTH_MAY,
      MONTH_JUNE,
      MONTH_JULY,
      MONTH_AUGUST,
      MONTH_SEPTEMBER,
      MONTH_OCTOBER,
      MONTH_NOVEMBER,
      MONTH_DECEMBER
    };

    enum WEEKDAY_t : uint8_t {
      WEEKDAY_SUNDAY = 1,
      WEEKDAY_MONDAY,
      WEEKDAY_TUESDAY,
      WEEKDAY_WEDNESDAY,
      WEEKDAY_THURSDAY,
      WEEKDAY_FRIDAY,
      WEEKDAY_SATURDAY
    };

  public:
    uint8_t getWeekday() const {
      return weekday;
    }

    uint8_t getDayByte() const {
      return day;
    }

    uint8_t getMonthByte() const {
      return month;
    }

    uint8_t getYearByte() const {
      return year;
    }

    uint8_t getDay() const {
      return timeFromByte(day, DATE_MASK);
    }

    uint8_t getMonth() const {
      return timeFromByte(month, MONTHS_MASK);
    }

    uint8_t getYear() const {
      return timeFromByte(year, YEARS_MASK);
    }

    uint8_t getDay1() const {
      return day & DATE_MASK_1;
    }

    uint8_t getMonth1() const {
      return month & MONTHS_MASK_1;
    }

    uint8_t getYear1() const {
      return year & YEARS_MASK_1;
    }

    uint8_t getDay10() const {
      return (day & DATE_MASK_10) >> SHIFT_10;
    }

    uint8_t getMonth10() const {
      return (month & MONTHS_MASK_10) >> SHIFT_10;
    }

    uint8_t getYear10() const {
      return (year & YEARS_MASK_10) >> SHIFT_10;
    }

    void getNow() {
      wireRequest(ADDRESS_DAY, ADDRESS_YEAR);
      weekday = wireRead();
      day = wireRead();
      month = wireRead();
      year = wireRead();
    }

    void setNow(uint8_t year, uint8_t month, uint8_t day, uint8_t weekday) {
      wireWrite(ADDRESS_YEAR, byteFromTime(year));
      wireWrite(ADDRESS_MONTH, byteFromTime(month));
      wireWrite(ADDRESS_DAY, byteFromTime(weekday));
      wireWrite(ADDRESS_DATE, byteFromTime(day));
    }
};
}

#endif

