#ifndef H_RTC_ALARM
#define H_RTC_ALARM

#include "rtc_ds3231.h"

#include <inttypes.h>

namespace RTC {
class Alarm : public DS3231 {
  private:
    static const uint8_t ALARM_BIT = 0x80;
    static const uint8_t ALARM_WEEKDAY_BIT = 0x40;

  protected:
    enum MATCH_t : uint8_t {
      ALARM_1_EVERY_SECOND = 0x0F,
      ALARM_1_MATCH_SECONDS = 0x0E,
      ALARM_1_MATCH_MINUTES = 0x0C,
      ALARM_1_MATCH_HOURS = 0x08,
      ALARM_1_MATCH_DAY = 0x00,
      ALARM_1_MATCH_WEEKDAY = 0x10,

      ALARM_2_EVERY_MINUTE = 0x8E,
      ALARM_2_MATCH_MINUTES = 0x8C,
      ALARM_2_MATCH_HOURS = 0x88,
      ALARM_2_MATCH_DAY = 0x80,
      ALARM_2_MATCH_WEEKDAY = 0x90
    };

    enum MATCH_BIT_t : uint8_t {
      MATCH_ALARM_2 = 0x80,
      MATCH_DYDT = 0x10,
      MATCH_DAY = 0x08,
      MATCH_HOURS = 0x04,
      MATCH_MINUTES = 0x02,
      MATCH_SECONDS = 0x01
    };

    void writeAlarmByteFromTime(
      uint8_t address, uint8_t time,
      bool alarmBit, bool weekdayBit = false
    ) {
      uint8_t value = byteFromTime(time);
      if (alarmBit) {
        value |= ALARM_BIT;
      }
      if (weekdayBit) {
        value |= ALARM_WEEKDAY_BIT;
      }
      wireWrite(address, value);
    }

    void setAlarm(
      uint8_t match,
      uint8_t seconds = 0, uint8_t minutes = 0, uint8_t hours = 0, uint8_t day = 0
    ) {
      uint8_t address;
      if (match & MATCH_ALARM_2) {
        address = ADDRESS_A2_MINUTES;
      } else {
        address = ADDRESS_A1_SECONDS;
        writeAlarmByteFromTime(address++, seconds, match & MATCH_SECONDS);
      }
      writeAlarmByteFromTime(address++, minutes, match & MATCH_MINUTES);
      writeAlarmByteFromTime(address++, hours, match & MATCH_HOURS);
      writeAlarmByteFromTime(address++, day, match & MATCH_DAY, match & MATCH_DYDT);

      wireSetBits(ADDRESS_CONTROL, CONTROL_ALARM_1);
    }
};

class Alarm1 : public Alarm {
  public:
    void everySecond() {
      setAlarm(ALARM_1_EVERY_SECOND);
    }

    void matchSeconds(uint8_t seconds) {
      setAlarm(ALARM_1_MATCH_SECONDS, seconds);
    }

    void matchMinutes(uint8_t seconds, uint8_t minutes) {
      setAlarm(ALARM_1_MATCH_MINUTES, seconds, minutes);
    }

    void matchHours(uint8_t seconds, uint8_t minutes, uint8_t hours) {
      setAlarm(ALARM_1_MATCH_HOURS, seconds, minutes, hours);
    }

    void matchDay(uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t day) {
      setAlarm(ALARM_1_MATCH_DAY, seconds, minutes, hours, day);
    }

    void matchWeekday(uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t day) {
      setAlarm(ALARM_1_MATCH_WEEKDAY, seconds, minutes, hours, day);
    }

    void unset() {
      wireClearBits(ADDRESS_CONTROL, CONTROL_ALARM_1);
    }

    void clear() {
      wireClearBits(ADDRESS_STATUS, STATUS_ALARM_1);
    }
};

class Alarm2 : public Alarm {
  public:
    void everyMinute() {
      setAlarm(ALARM_2_EVERY_MINUTE);
    }

    void matchMinutes(uint8_t minutes) {
      setAlarm(ALARM_2_MATCH_MINUTES, 0, minutes);
    }

    void matchHours(uint8_t minutes, uint8_t hours) {
      setAlarm(ALARM_2_MATCH_HOURS, 0, minutes, hours);
    }

    void matchDay(uint8_t minutes, uint8_t hours, uint8_t day) {
      setAlarm(ALARM_2_MATCH_DAY, 0, minutes, hours, day);
    }

    void matchWeekday(uint8_t minutes, uint8_t hours, uint8_t day) {
      setAlarm(ALARM_2_MATCH_WEEKDAY, 0, minutes, hours, day);
    }

    void unset() {
      wireClearBits(ADDRESS_CONTROL, CONTROL_ALARM_2);
    }

    void clear() {
      wireClearBits(ADDRESS_STATUS, STATUS_ALARM_2);
    }
};
}

#endif

