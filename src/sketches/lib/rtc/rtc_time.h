#ifndef H_RTC_TIME
#define H_RTC_TIME

#include "rtc_ds3231.h"

#include <inttypes.h>

namespace RTC {
class Time : public DS3231 {

  private:
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;

  public:
    uint8_t getHoursByte() const {
      return hours;
    }

    uint8_t getMinutesByte() const {
      return minutes;
    }

    uint8_t getSecondsByte() const {
      return seconds;
    }

    uint8_t getHours() const {
      return timeFromByte(hours, HOURS_MASK);
    }

    uint8_t getMinutes() const {
      return timeFromByte(minutes, MINUTES_MASK);
    }

    uint8_t getSeconds() const {
      return timeFromByte(seconds, SECONDS_MASK);
    }

    uint8_t getHours1() const {
      return hours & HOURS_MASK_1;
    }

    uint8_t getMinutes1() const {
      return minutes & MINUTES_MASK_1;
    }

    uint8_t getSeconds1() const {
      return seconds & SECONDS_MASK_1;
    }

    uint8_t getHours10() const {
      return (hours & HOURS_MASK_10) >> SHIFT_10;
    }

    uint8_t getMinutes10() const {
      return (minutes & MINUTES_MASK_10) >> SHIFT_10;
    }

    uint8_t getSeconds10() const {
      return (seconds & SECONDS_MASK_10) >> SHIFT_10;
    }

    void getNow() {
      wireRequest(ADDRESS_SECONDS, ADDRESS_HOURS);
      seconds = wireRead();
      minutes = wireRead();
      hours = wireRead();
    }

    void setNow(uint8_t hours, uint8_t minutes, uint8_t seconds) {
      wireWrite(ADDRESS_HOURS, byteFromTime(hours));
      wireWrite(ADDRESS_MINUTES, byteFromTime(minutes));
      wireWrite(ADDRESS_SECONDS, byteFromTime(seconds));
    }
};
}

#endif

