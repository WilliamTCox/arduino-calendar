#ifndef H_RTC_DS3231
#define H_RTC_DS3231

#include <inttypes.h>
#include <Wire.h>

namespace RTC {
class DS3231 {
  private:
    static const uint8_t ADDRESS = 0x68;

  protected:
    enum ADDRESS_t : uint8_t {
      ADDRESS_SECONDS,
      ADDRESS_MINUTES,
      ADDRESS_HOURS,
      ADDRESS_DAY,
      ADDRESS_DATE,
      ADDRESS_MONTH,
      ADDRESS_YEAR,
      ADDRESS_A1_SECONDS,
      ADDRESS_A1_MINUTES,
      ADDRESS_A1_HOURS,
      ADDRESS_A1_DAY,
      ADDRESS_A2_MINUTES,
      ADDRESS_A2_HOURS,
      ADDRESS_A2_DAY,
      ADDRESS_CONTROL,
      ADDRESS_STATUS,
      ADDRESS_AGING_OFFSET,
      ADDRESS_TEMP_MSB,
      ADDRESS_TEMP_LSB
    };

    enum CONTROL_BIT_t : uint8_t {
      CONTROL_BATTERY_OSCILLATOR_STOP = 0x80,
      CONTROL_BATTERY_WAVE = 0x40,
      CONTROL_CONVERT_TEMP = 0x20,
      CONTROL_RATE_1 = 0x10,
      CONTROL_RATE_2 = 0x08,
      CONTROL_INTERRUPT = 0x04,
      CONTROL_ALARM_2 = 0x02,
      CONTROL_ALARM_1 = 0x01
    };

    enum STATUS_BIT_t : uint8_t {
      STATUS_OSCILLATOR_STOPPED = 0x80,
      STATUS_ENABLE_32KHZ = 0x08,
      STATUS_BUSY = 0x04,
      STATUS_ALARM_2 = 0x02,
      STATUS_ALARM_1 = 0x01
    };

    static const uint8_t TIME_MASK_1 = 0x0F;
    static const uint8_t SECONDS_MASK_1 = 0x0F;
    static const uint8_t MINUTES_MASK_1 = 0x0F;
    static const uint8_t HOURS_MASK_1 = 0x0F;
    static const uint8_t DATE_MASK_1 = 0x0F;
    static const uint8_t MONTHS_MASK_1 = 0x0F;
    static const uint8_t YEARS_MASK_1 = 0x0F;

    static const uint8_t TIME_MASK_10 = 0xF0;
    static const uint8_t SECONDS_MASK_10 = 0x70;
    static const uint8_t MINUTES_MASK_10 = 0x70;
    static const uint8_t HOURS_MASK_10 = 0x30;
    static const uint8_t DATE_MASK_10 = 0x30;
    static const uint8_t MONTHS_MASK_10 = 0x10;
    static const uint8_t YEARS_MASK_10 = 0xF0;

    static const uint8_t TIME_MASK = TIME_MASK_1 | TIME_MASK_10;
    static const uint8_t SECONDS_MASK = SECONDS_MASK_1 | SECONDS_MASK_10;
    static const uint8_t MINUTES_MASK = MINUTES_MASK_1 | MINUTES_MASK_10;
    static const uint8_t HOURS_MASK = HOURS_MASK_1 | HOURS_MASK_10;
    static const uint8_t DATE_MASK = DATE_MASK_1 | DATE_MASK_10;
    static const uint8_t MONTHS_MASK = MONTHS_MASK_1 | MONTHS_MASK_10;
    static const uint8_t YEARS_MASK = YEARS_MASK_1 | YEARS_MASK_10;

    static const uint8_t SHIFT_10 = 4;

    uint8_t timeFromByte(uint8_t value, uint8_t mask = TIME_MASK) const {
      uint8_t masked = value & mask;
      return masked - 6 * (masked / 16);
    }

    uint8_t byteFromTime(uint8_t value) const {
      return value + 6 * (value / 10);
    }

    void wireWrite(uint8_t address, uint8_t value) {
      Wire.beginTransmission(ADDRESS);
      Wire.write(address);
      Wire.write(value);
      Wire.endTransmission();
    }

    void wireRequest(uint8_t first, uint8_t last) {
      Wire.beginTransmission(ADDRESS);
      Wire.write(first);
      Wire.endTransmission();

      uint8_t width = last - first + 1;
      Wire.requestFrom(ADDRESS, width);
    }

    uint8_t wireRequest(uint8_t address) {
      wireRequest(address, address);
      return Wire.read();
    }

    uint8_t wireRead() {
      return Wire.read();
    }

    void wireWriteBits(uint8_t address, uint8_t setMask, uint8_t clearMask) {
      uint8_t value = wireRequest(address);
      value |= setMask;
      value &= ~clearMask;
      wireWrite(address, value);
    }

    void wireSetBits(uint8_t address, uint8_t setMask) {
      wireWriteBits(address, setMask, 0);
    }

    void wireClearBits(uint8_t address, uint8_t clearMask) {
      wireWriteBits(address, 0, clearMask);
    }

    static char * pstrCopy(char *dest, const char *src) {
      while ((*dest++ = pgm_read_byte_near(src++))) {}
      return --dest;
    }

  public:
    static void setup() {
      DS3231 rtc;
      for (int address = ADDRESS_A1_SECONDS; address <= ADDRESS_AGING_OFFSET; ++address) {
        rtc.wireWrite(address, 0);
      }
      rtc.wireWrite(ADDRESS_CONTROL, CONTROL_INTERRUPT);
    }
};
}

#endif

