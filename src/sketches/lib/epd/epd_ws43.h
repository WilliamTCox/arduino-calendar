#ifndef H_EPD_WS43
#define H_EPD_WS43

#include <limits.h>

namespace EPD {
class WS43 {
  public:
    static const uint16_t WIDTH = 800;
    static const uint16_t HEIGHT = 600;

  protected:
    typedef uint16_t FRAME_SIZE_t;

  private:
    static const uint8_t WAKE_UP_PIN = 3;
    static const uint8_t RESET_PIN = 4;

    static const uint8_t FRAME_HEADER = 0xA5;
    static const uint32_t FRAME_END = 0xCC33C33C;

  protected:
    enum COMMAND_t : uint8_t {
      // System control
      COMMAND_HANDSHAKE = 0x00,
      COMMAND_SET_BAUD = 0x01,
      COMMAND_READ_BAUD = 0x02,
      COMMAND_READ_STORAGE = 0x06,
      COMMAND_SET_STORAGE = 0x07,
      COMMAND_SLEEP = 0x08,
      COMMAND_UPDATE = 0x0A,
      COMMAND_READ_ROTATION = 0x0C,
      COMMAND_SET_ROTATION = 0x0D,
      COMMAND_IMPORT_FONT = 0x0E,
      COMMAND_IMPORT_IMAGE = 0x0F,

      // Display parameter configuration
      COMMAND_SET_COLOR = 0x10,
      COMMAND_READ_COLOR = 0x11,
      COMMAND_READ_EN_FONT = 0x1C,
      COMMAND_READ_CH_FONT = 0x1D,
      COMMAND_SET_EN_FONT = 0x1E,
      COMMAND_SET_CH_FONT = 0x1F,

      // Basic drawings
      COMMAND_DRAW_POINT = 0x20,
      COMMAND_DRAW_LINE = 0x22,
      COMMAND_FILL_RECTANGLE = 0x24,
      COMMAND_DRAW_RECTANGLE = 0x25,
      COMMAND_DRAW_CIRCLE = 0x26,
      COMMAND_FILL_CIRCLE = 0x27,
      COMMAND_DRAW_TRIANGLE = 0x28,
      COMMAND_FILL_TRIANGLE = 0x29,
      COMMAND_CLEAR = 0x2E,

      // Display text
      COMMAND_DRAW_STRING = 0x30,

      // Display image
      COMMAND_DRAW_IMAGE = 0x70
    };

  public:
    enum COLOR_t : uint8_t {
      COLOR_BLACK = 0x00,
      COLOR_DARK_GRAY = 0x01,
      COLOR_LIGHT_GRAY = 0x02,
      COLOR_WHITE = 0x03
    };

    enum STORAGE_t : uint8_t {
      STORAGE_NAND = 0x00,
      STORAGE_TF = 0x01
    };

    enum FONT_t : uint8_t {
      FONT_32 = 0x01,
      FONT_48 = 0x02,
      FONT_64 = 0x03
    };

    enum ROTATION_t : uint8_t {
      ROTATION_NORMAL = 0x00,
      ROTATION_180 = 0x01
    };

  private:
    uint8_t parity;
    static const FRAME_SIZE_t FRAME_SIZE = sizeof(FRAME_HEADER) +
                                           sizeof(FRAME_SIZE_t) +
                                           sizeof(COMMAND_t) +
                                           sizeof(FRAME_END) +
                                           sizeof(parity);

    void serial_write_byte(uint8_t data) {
      Serial.write(data);
      parity ^= data;
    }

    template<typename T>
    void serial_write(T data) {
      uint8_t *data8 = reinterpret_cast<uint8_t *>(&data);
      for (size_t i = sizeof(data); i > 0; --i) {
        serial_write_byte(data8[i - 1]);
      }
    }

    void serial_write(const char *string) {
      while (*string) {
        serial_write_byte(*string++);
      }
      serial_write_byte(*string);
    }

    void startCommand(COMMAND_t command, FRAME_SIZE_t dataSize = 0) {
      parity = 0;
      serial_write(FRAME_HEADER);
      serial_write(FRAME_SIZE + dataSize);
      serial_write(command);
    }

    void endCommand() {
      serial_write(FRAME_END);
      serial_write(parity);
    }

  protected:
    void sendCommand(COMMAND_t command) {
      startCommand(command);
      endCommand();
    }

    template<typename T>
    void sendCommand(COMMAND_t command, T data) {
      startCommand(command, sizeof(data));
      serial_write(data);
      endCommand();
    }

    template<typename T1, typename T2>
    void sendCommand(COMMAND_t command, T1 data1, T2 data2) {
      startCommand(command, sizeof(data1) + sizeof(data2));
      serial_write(data1);
      serial_write(data2);
      endCommand();
    }

    template<typename T1, typename T2>
    void sendCommand(COMMAND_t command, T1 data1, T2 data2, const char *string) {
      startCommand(command, sizeof(data1) + sizeof(data2) + strlen(string) + 1);
      serial_write(data1);
      serial_write(data2);
      serial_write(string);
      endCommand();
    }

  public:
    static void setup() {
      pinMode(WAKE_UP_PIN, OUTPUT);
      digitalWrite(WAKE_UP_PIN, LOW);

      pinMode(RESET_PIN, OUTPUT);
      digitalWrite(RESET_PIN, LOW);
    }

    void wakeUp() {
      digitalWrite(WAKE_UP_PIN, LOW);
      delayMicroseconds(10);
      digitalWrite(WAKE_UP_PIN, HIGH);
      delayMicroseconds(500);
      digitalWrite(WAKE_UP_PIN, LOW);
      delay(10);
    }
};
}

#endif

