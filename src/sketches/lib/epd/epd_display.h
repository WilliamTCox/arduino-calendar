#ifndef H_EPD_DISPLAY
#define H_EPD_DISPLAY

#include "epd_ws43.h"

namespace EPD {
class Display : public WS43 {
  public:
    void setStorage(WS43::STORAGE_t storage) {
      sendCommand(COMMAND_SET_STORAGE, storage);
    }

    void setColor(COLOR_t foreground, COLOR_t background) {
      sendCommand(COMMAND_SET_COLOR, foreground, background);
    }

    void clear() {
      sendCommand(COMMAND_CLEAR);
    }

    void update() {
      sendCommand(COMMAND_UPDATE);
    }

    void setChineseFont(FONT_t font) {
      sendCommand(COMMAND_SET_CH_FONT, font);
    }

    void setEnglishFont(FONT_t font) {
      sendCommand(COMMAND_SET_EN_FONT, font);
    }

    void drawString(uint16_t x, uint16_t y, const char *string) {
      sendCommand(COMMAND_DRAW_STRING, x, y, string);
    }

    void drawImage(uint16_t x, uint16_t y, const char *filename) {
      sendCommand(COMMAND_DRAW_IMAGE, x, y, filename);
    }

    void sleep() {
      sendCommand(COMMAND_SLEEP);
    }
};
}
#endif

