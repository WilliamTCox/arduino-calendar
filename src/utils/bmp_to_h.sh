#!/bin/sh

h_name=$1
filename_prefix=$2
first_bmp=$3
shift 3

echo_enum_line () {
  line='      '
  if [ "$2" != 'first' ]
  then
    line="${line},"
  fi

  digit_suffix=$(echo "$1" | sed 's/^[^[:digit:]]*//')
  if [ -n "${digit_suffix}" ]
  then
    line="${line}${h_name}_${digit_suffix}"
  else
    line="${line}$1"
  fi

  echo "${line}"
}

echo "#ifndef H_IMAGES_${h_name}"
echo "#define H_IMAGES_${h_name}"
echo '#include "image.h"'

echo 'namespace Images {'
echo "  class ${h_name} : public Image {"
echo '  public:'

echo '    enum index_t : uint8_t {'
echo_enum_line "${first_bmp}" first
for bmp; do
  echo_enum_line "${bmp}"
done
echo '    };'

echo '    virtual uint16_t width(uint8_t n) {'
echo '      return Image::width(widths, n);'
echo '    }'

echo '    virtual const char * filename(uint8_t n) {'
echo "      return Image::filename(PSTR(\"${filename_prefix}\"), n);"
echo '    }'

echo '  protected:'
echo "    static const PROGMEM uint16_t widths[$(($# + 1))];"
echo '  };'
echo '}'

echo '#endif'
