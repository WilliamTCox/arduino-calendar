#ifndef H_ARDUINO_CALENDAR_PNG
#define H_ARDUINO_CALENDAR_PNG

#include <ios>
#include <vector>

#include <png.h>

class PNG {
protected:
  static const size_t PNG_SIG_SIZE = 8;

protected:
  png_structp png;
  png_infop   info;

  std::vector<uint8_t *> rowPointers;
  std::vector<uint8_t>   imageData;

  void destroy() {
    png_destroy_read_struct(&png, &info, nullptr);
  }

public:
  explicit PNG(std::istream &in);

  ~PNG() {
    destroy();
  }

  const uint8_t *const *getRowPointers() {
    return &rowPointers[0];
  }

  png_uint_32 getWidth() {
    return png_get_image_width(png, info);
  }

  png_uint_32 getHeight() {
    return png_get_image_height(png, info);
  }

  png_size_t getRowBytes() {
    return png_get_rowbytes(png, info);
  }

protected:
  static bool validate(std::istream &in);

  static void read(png_structp png, png_bytep data, png_size_t length);

  static void error(png_structp png, png_const_charp message);

  static void warning(png_structp png, png_const_charp message);
};

#endif
