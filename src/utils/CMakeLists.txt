cmake_minimum_required(VERSION 3.9)
project(utils)

pkg_check_modules(LIBPNG libpng16 REQUIRED)
link_directories(${LIBPNG_LIBRARY_DIRS})

add_library(bitmap bitmap/bitmap.cpp bitmap/bmp_header.cpp bitmap/dib_header.cpp bitmap/color_table.cpp bitmap/pixel_data.cpp)

add_executable(png_to_bmp png_to_bmp.cpp png.cpp png.h)
target_link_libraries(png_to_bmp bitmap)

target_include_directories(png_to_bmp PRIVATE ${LIBPNG_INCLUDE_DIRS})
target_compile_options(png_to_bmp PRIVATE ${LIBPNG_CFLAGS_OTHER})
target_link_libraries(png_to_bmp PRIVATE ${LIBPNG_LDLAGS_OTHER})
target_link_libraries(png_to_bmp ${LIBPNG_LIBRARIES})


add_custom_command(
    OUTPUT ${utils_BINARY_DIR}/bmp_to_h.sh
    COMMAND cp ${utils_SOURCE_DIR}/bmp_to_h.sh ${utils_BINARY_DIR}
    DEPENDS ${utils_SOURCE_DIR}/bmp_to_h.sh
    COMMENT "Copying bmp_to_h.sh"
)
add_custom_target(bmp_to_h DEPENDS ${utils_BINARY_DIR}/bmp_to_h.sh)
set(bmp_to_h_target bmp_to_h ${utils_BINARY_DIR}/bmp_to_h.sh PARENT_SCOPE)

add_custom_command(
    OUTPUT ${utils_BINARY_DIR}/bmp_to_cpp.sh
    COMMAND cp ${utils_SOURCE_DIR}/bmp_to_cpp.sh ${utils_BINARY_DIR}
    DEPENDS ${utils_SOURCE_DIR}/bmp_to_cpp.sh
    COMMENT "Copying bmp_to_cpp.sh"
)
add_custom_target(bmp_to_cpp DEPENDS ${utils_BINARY_DIR}/bmp_to_cpp.sh)
set(bmp_to_cpp_target bmp_to_cpp ${utils_BINARY_DIR}/bmp_to_cpp.sh PARENT_SCOPE)
