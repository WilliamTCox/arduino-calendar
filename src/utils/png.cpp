#include "png.h"

#include <iostream>


PNG::PNG(std::istream &in)
: png(nullptr), info(nullptr), rowPointers(), imageData() {
  if (!validate(in)) {
    throw std::invalid_argument("Not a valid PNG stream");
  }

  png = png_create_read_struct(PNG_LIBPNG_VER_STRING, this, error, warning);
  if (!png) {
    throw std::runtime_error("Failed to allocate png_struct");
  }

  info = png_create_info_struct(png);
  if (!info) {
    destroy();
    throw std::runtime_error("Failed to allocate info png_info");
  }

  png_set_read_fn(png, &in, read);

  png_set_sig_bytes(png, PNG_SIG_SIZE);
  png_read_info(png, info);

  auto height   = getHeight();
  auto rowBytes = getRowBytes();
  imageData.resize(height * getRowBytes());
  for (png_size_t i = 0; i < height; ++i) {
    rowPointers.push_back(&imageData[i * rowBytes]);
  }

  if (png_get_bit_depth(png, info) == 16) {
    png_set_strip_16(png);
  }
  if (png_get_color_type(png, info) & PNG_COLOR_MASK_ALPHA) {
    png_set_strip_alpha(png);
  }
  if (png_get_bit_depth(png, info) < 8) {
    png_set_packing(png);
  }
  if ((png_get_color_type(png, info) == PNG_COLOR_TYPE_RGB) ||
      (png_get_color_type(png, info) == PNG_COLOR_TYPE_RGB_ALPHA)) {
    png_set_rgb_to_gray_fixed(png, 1, -1, -1);
  }

  png_read_image(png, &rowPointers[0]);
  png_read_end(png, nullptr);
}

bool PNG::validate(std::istream &in) {
  png_byte pngSig[PNG_SIG_SIZE];
  in.read(reinterpret_cast<char *>(pngSig), sizeof(pngSig));

  return in.good() && png_check_sig(pngSig, PNG_SIG_SIZE);
}

void PNG::read(png_structp png, png_bytep data, png_size_t length) {
  auto *in = reinterpret_cast<std::istream *>(png_get_io_ptr(png));
  in->read(reinterpret_cast<char *>(data), length);
}

void PNG::error(png_structp png, png_const_charp message) {
  std::cerr << message << std::endl;
  throw std::runtime_error("Error");
}

void PNG::warning(png_structp png, png_const_charp message) {
  std::cerr << message << std::endl;
  throw std::runtime_error("Warning");
}
