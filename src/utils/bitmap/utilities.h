#ifndef H_ARDUINO_CALENDAR_BITMAP_UTILITIES
#define H_ARDUINO_CALENDAR_BITMAP_UTILITIES

#include <ostream>
#include <climits>

namespace Bitmap {
  template<typename T>
  struct LittleEndian {
    T data;
  };

  template<typename T>
  LittleEndian<T> little(T data) {
    return {data};
  }

  template<typename T, typename CharT, typename Traits>
  std::basic_ostream<CharT, Traits> &operator<<(std::basic_ostream<CharT, Traits> &out, LittleEndian<T> le) {
    for (size_t i = 0; i < sizeof(le.data); ++i) {
      out.put(le.data);
      le.data >>= CHAR_BIT;
    }
    return out;
  }

  template<typename T>
  struct BigEndian {
    T data;
  };

  template<typename T>
  BigEndian<T> big(T data) {
    return {data};
  }

  template<typename T, typename CharT, typename Traits>
  std::basic_ostream<CharT, Traits> &operator<<(std::basic_ostream<CharT, Traits> &out, BigEndian<T> be) {
    size_t      byteShift = CHAR_BIT * (sizeof(be.data) - 1);
    for (size_t i         = 0; i < sizeof(be.data); ++i) {
      out.put(be.data >> byteShift);
      be.data <<= CHAR_BIT;
    }
    return out;
  }
}

#endif
