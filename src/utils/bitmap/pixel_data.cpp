#include "pixel_data.h"

#include "utilities.h"


typedef uint32_t PixelBlock;

uint32_t Bitmap::PixelData::getBytesPerRow() const {
  auto bitsPerRow = dibHeader.getWidth() * dibHeader.getBitsPerPixel();
  auto blockBits  = sizeof(PixelBlock) * CHAR_BIT;

  auto totalBlocks = ((bitsPerRow + blockBits - 1) / blockBits);

  return totalBlocks * sizeof(PixelBlock);
}

void Bitmap::PixelData::write(std::ostream &out) const {
  for (size_t i = 0; i < dibHeader.getHeight(); ++i) {
    size_t reverse = dibHeader.getHeight() - i - 1;
    writeRow(out, imageData[reverse]);
  }
}

void Bitmap::PixelData::writeRow(std::ostream &out, const uint8_t *pixels) const {
  size_t pixelsPerBlock = CHAR_BIT * sizeof(PixelBlock) / dibHeader.getBitsPerPixel();

  for (size_t i = 0; i < dibHeader.getWidth();) {
    PixelBlock data = 0;

    for (size_t j = 0; j < pixelsPerBlock; ++j) {
      data <<= dibHeader.getBitsPerPixel();
      if (i < dibHeader.getWidth()) {
        data |= dibHeader.getColorTable().getBMPColor(pixels[i++]);
      } else {
        data |= ColorTable::WHITE_INDEX;
      }
    }

    out << big(data);
  }
}
