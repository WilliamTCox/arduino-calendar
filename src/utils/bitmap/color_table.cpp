#include "color_table.h"

#include <ostream>


static const uint8_t BLACK      = 0x00;
static const uint8_t DARK_GRAY  = 0x55;
static const uint8_t LIGHT_GRAY = 0xAA;
static const uint8_t WHITE      = 0xFF;


static constexpr uint8_t imageShadeCutoff(uint8_t lowerShade, uint8_t upperShade) noexcept {
  return static_cast<uint8_t>((15 * lowerShade + upperShade) / 16);
}

static const double BLACK_CUTOFF      = imageShadeCutoff(BLACK, DARK_GRAY);
static const double DARK_GRAY_CUTOFF  = imageShadeCutoff(DARK_GRAY, LIGHT_GRAY);
static const double LIGHT_GRAY_CUTOFF = imageShadeCutoff(LIGHT_GRAY, WHITE);


void Bitmap::ColorTable::writeGray(std::ostream &out, uint8_t gray) const {
  out.put(gray);
  out.put(gray);
  out.put(gray);
  out.put(0x00);
}

void Bitmap::ColorTable::write(std::ostream &out) const {
  writeGray(out, BLACK);
  writeGray(out, DARK_GRAY);
  writeGray(out, LIGHT_GRAY);
  writeGray(out, WHITE);
}

uint8_t Bitmap::ColorTable::getBMPColor(uint8_t shade) const {
  if (shade < BLACK_CUTOFF) {
    return BLACK_INDEX;
  } else if (shade < DARK_GRAY_CUTOFF) {
    return DARK_GRAY_INDEX;
  } else if (shade < LIGHT_GRAY_CUTOFF) {
    return LIGHT_GRAY_INDEX;
  } else {
    return WHITE_INDEX;
  }
}
