#ifndef H_ARDUINO_CALENDAR_BITMAP_BITMAP
#define H_ARDUINO_CALENDAR_BITMAP_BITMAP

#include <cstdint>
#include <ios>

#include "bmp_header.h"

namespace Bitmap {
  class Bitmap {
  protected:
    ColorTable colorTable;
    DIBHeader  dibHeader;
    PixelData  pixelData;
    BMPHeader  bmpHeader;

  public:
    Bitmap(uint32_t width, uint32_t height, const uint8_t *const *imageData)
        : colorTable(),
          dibHeader(width, height, colorTable),
          pixelData(dibHeader, imageData),
          bmpHeader(dibHeader, pixelData) {
    }

    void write(std::ostream &out) const;
  };
}

#endif
