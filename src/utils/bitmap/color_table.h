#ifndef H_ARDUINO_CALENDAR_BITMAP_COLOR_TABLE
#define H_ARDUINO_CALENDAR_BITMAP_COLOR_TABLE

#include <ios>
#include <cstdint>

namespace Bitmap {
  class ColorTable {
  protected:
    static const uint16_t NUMBER_OF_COLORS = 4;
    static const uint32_t SIZE             = NUMBER_OF_COLORS * 4;

  public:
    enum COLOR_INDEX_t : uint8_t {
      BLACK_INDEX, DARK_GRAY_INDEX, LIGHT_GRAY_INDEX, WHITE_INDEX
    };

  protected:
    void writeGray(std::ostream &out, uint8_t gray) const;

  public:
    void write(std::ostream &out) const;

    auto getNumberOfColors() const {
      return NUMBER_OF_COLORS;
    }

    auto getSize() const {
      return SIZE;
    }

    uint8_t getBMPColor(uint8_t shade) const;
  };
}

#endif
