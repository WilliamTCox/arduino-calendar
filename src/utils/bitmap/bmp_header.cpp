#include "bmp_header.h"

#include "utilities.h"


static const char     HEADER[2]   = {'B', 'M'};
static const uint16_t RESERVED[2] = {0, 0};


void Bitmap::BMPHeader::write(std::ostream &out) const {
  for (auto data : HEADER) {
    out << little(data);
  }
  out << little(getFileSize());
  for (auto data : RESERVED) {
    out << little(data);
  }
  out << little(getDataOffset());
}
