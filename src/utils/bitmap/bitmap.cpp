#include "bitmap.h"


void Bitmap::Bitmap::write(std::ostream &out) const {
  bmpHeader.write(out);
  dibHeader.write(out);
  colorTable.write(out);
  pixelData.write(out);
}
