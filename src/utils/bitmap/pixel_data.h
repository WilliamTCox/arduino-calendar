#ifndef H_ARDUINO_CALENDAR_BITMAP_PIXEL_DATA
#define H_ARDUINO_CALENDAR_BITMAP_PIXEL_DATA

#include <cstdint>
#include <ios>

#include "dib_header.h"

namespace Bitmap {
  class PixelData {
  protected:
    const uint8_t *const *imageData;
    DIBHeader &dibHeader;

  public:
    explicit PixelData(DIBHeader &dibHeader, const uint8_t *const *imageData)
        : imageData(imageData), dibHeader(dibHeader) {
    }

    uint32_t getBytesPerRow() const;

    uint32_t getSize() const {
      return getBytesPerRow() * dibHeader.getHeight();
    }

    void write(std::ostream &out) const;

  protected:
    void writeRow(std::ostream &out, const uint8_t *pixels) const;
  };
}

#endif
