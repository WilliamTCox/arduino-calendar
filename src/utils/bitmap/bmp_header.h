#ifndef H_ARDUINO_CALENDAR_BITMAP_BMP_HEADER
#define H_ARDUINO_CALENDAR_BITMAP_BMP_HEADER

#include <cstdint>
#include <ios>

#include "dib_header.h"
#include "pixel_data.h"

namespace Bitmap {
  class BMPHeader {
  protected:
    static const uint32_t SIZE = 14;

  protected:
    DIBHeader &dibHeader;
    PixelData &pixelData;

  public:
    BMPHeader(DIBHeader &dibHeader, PixelData &pixelData)
        : dibHeader(dibHeader), pixelData(pixelData) {
    }

    uint32_t getDataOffset() const {
      return SIZE + dibHeader.getHeaderSize();
    }

    uint32_t getFileSize() const {
      return getDataOffset() + pixelData.getSize();
    }

    void write(std::ostream &out) const;
  };
}

#endif
