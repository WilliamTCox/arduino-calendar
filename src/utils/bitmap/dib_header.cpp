#include "dib_header.h"

#include <cmath>

#include "utilities.h"


static const uint16_t NUMBER_OF_COLOR_PLANES     = 1;
static const uint32_t COMPRESSION                = 0;
static const uint32_t IMAGE_SIZE                 = 0;
static const uint32_t HORIZONTAL_RESOLUTION      = 0;
static const uint32_t VERTICAL_RESOLUTION        = 0;
static const uint32_t NUMBER_OF_IMPORTANT_COLORS = 0;


void Bitmap::DIBHeader::write(std::ostream &out) const {
  out
      << little(SIZE)
      << little(getWidth() + (getWidth() % 2))
      << little(getHeight())
      << little(NUMBER_OF_COLOR_PLANES)
      << little(getBitsPerPixel())
      << little(COMPRESSION)
      << little(IMAGE_SIZE)
      << little(HORIZONTAL_RESOLUTION)
      << little(VERTICAL_RESOLUTION)
      << little(getNumberOfColors())
      << little(NUMBER_OF_IMPORTANT_COLORS);
}

uint32_t Bitmap::DIBHeader::getNumberOfColors() const {
  auto maxColors = std::round(std::pow(2, getBitsPerPixel()));

  auto colorsNeeded  = colorTable.getNumberOfColors();
  auto colorsDefault = static_cast<uint32_t>(maxColors);

  return colorsNeeded == colorsDefault ? 0 : colorsNeeded;
}
