#ifndef H_ARDUINO_CALENDAR_BITMAP_DIB_HEADER
#define H_ARDUINO_CALENDAR_BITMAP_DIB_HEADER

#include <ios>
#include <cstdint>

#include "color_table.h"

namespace Bitmap {
  class DIBHeader {
  protected:
    static const uint32_t SIZE           = 40;
    static const uint16_t BITS_PER_PIXEL = 4;

  protected:
    uint32_t width;
    uint32_t height;

    ColorTable &colorTable;

  public:
    DIBHeader(uint32_t width, uint32_t height, ColorTable &colorTable)
        : width(width), height(height), colorTable(colorTable) {
    }

    void write(std::ostream &out) const;

    auto getSize() const {
      return SIZE;
    }

    auto getHeaderSize() const {
      return getSize() + colorTable.getSize();
    }

    auto getBitsPerPixel() const {
      return BITS_PER_PIXEL;
    }

    auto getWidth() const {
      return width;
    }

    auto getHeight() const {
      return height;
    }

    const auto &getColorTable() const {
      return colorTable;
    }

  protected:
    uint32_t getNumberOfColors() const;
  };
}

#endif
