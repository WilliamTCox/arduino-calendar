#!/bin/sh

h_name=$1
filename_prefix=$2
#first_bmp=$3
shift 3

h_name_lower=$(echo ${h_name} | tr '[:upper:]' '[:lower:]')

echo_array_line () {
  line='  '
  if [ "$2" != 'first' ]
  then
    line="${line},"
  fi

  bmp_width=$(od -A n --endian=little -j 0x12 -N 4 -t d4 "$1" | sed 's/^[[:space:]]*//')
  line="${line}${bmp_width}"

  echo "${line}"
}

echo "#include \"${h_name_lower}.h\""

echo "const PROGMEM uint16_t Images::${h_name}::widths[$(($# + 1))] = {"
echo_array_line "${filename_prefix}0.BMP" first
i=1
while [ "$i" -ne $# ]; do
  i=$((i + 1))
  echo_array_line "${filename_prefix}${i}.BMP"
done
echo '};'
