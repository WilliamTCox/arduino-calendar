#include <fstream>

#include "png.h"
#include "bitmap/bitmap.h"


int main(int argc, const char *const *argv) {
  if (argc < 2) {
    return EXIT_FAILURE;
  }

  const char *filenameIn  = argv[1];
  const char *filenameOut = argv[2];

  std::ifstream fileIn(filenameIn, std::ios::in | std::ios::binary);
  PNG           image(fileIn);

  auto imageWidth  = static_cast<uint32_t>(image.getWidth());
  auto imageHeight = static_cast<uint32_t>(image.getHeight());
  auto rowPointers = image.getRowPointers();

  Bitmap::Bitmap bmp(imageWidth, imageHeight, rowPointers);

  std::ofstream fileOut(filenameOut, std::ios::out | std::ios::trunc | std::ios::binary);
  bmp.write(fileOut);
  fileOut.close();

  return 0;
}
