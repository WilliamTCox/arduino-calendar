#ifndef H_IMAGE
#define H_IMAGE

#include <avr/pgmspace.h>


namespace Images {
  static const size_t   FILENAME_LENGTH = 10;
  static const uint16_t LINE_HEIGHT     = 64;

  class Image {
  public:
    virtual uint16_t width(uint8_t n) = 0;

    virtual const char *filename(uint8_t imageNumber) = 0;

  protected:
    static uint16_t width(const uint16_t *widths, size_t n) {
      return pgm_read_word(&widths[n]);
    }

    static const char *filename(const char *prefix, uint8_t imageNumber) {
      static char name[FILENAME_LENGTH + 1];

      char   *dest = name;
      size_t next  = 0;
      size_t len   = sizeof(name);

      strncpy_P(dest, prefix, len);
      next += strnlen_P(prefix, len);

      if (next < len) {
        next += uint8ToA(dest + next, imageNumber, len - next - 1);
      }
      if (next < len) {
        strncpy_P(dest + next, PSTR(".BMP"), len - next - 1);
      }

      name[sizeof(name) - 1] = '\0';

      return name;
    }

    static size_t uint8ToA(char *dest, uint8_t number, size_t len) {
      size_t written = 0;

      uint8_t n = number;
      if (written < len && n >= 100) {
        dest[written++] = '0' + n / 100;
        n %= 100;
      }
      if (written < len && (n >= 10 || number >= 100)) {
        dest[written++] = '0' + n / 10;
        n %= 10;
      }
      if (written < len) {
        dest[written++] = '0' + n;
      }
      if (written < len) {
        dest[written] = '\0';
      }

      return written;
    }
  };
}

#endif
