#!/bin/sh

count=$1
width=$2

echo '#ifndef H_IMAGES_BODY'
echo '#define H_IMAGES_BODY'
echo '#include "image.h"'
echo 'namespace Images {'
echo '  class BODY : public Image {'
echo '  public:'
echo "    static const uint8_t COUNT = ${count};"
echo '    virtual uint16_t width(uint8_t n) {'
echo "      return ${width};"
echo '    }'
echo '    virtual const char * filename(uint8_t n) {'
echo '      return Image::filename(PSTR("B"), n);'
echo '    }'
echo '  };'
echo '}'
echo '#endif'
